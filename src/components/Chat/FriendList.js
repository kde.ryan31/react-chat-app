import React from "react";
import { useDispatch, useSelector } from "react-redux";

import styled from "styled-components";
import { AiFillCaretDown, AiFillCaretUp } from "react-icons/ai";

import Container from "../shared/Container";
import Button from "../shared/Button";
import Image from "../shared/Image";
import List from "../shared/List";
import Ul from "../shared/Ul";

import roomData from "../../data/room_data.json";

import { toggleSortBtn, showChatView } from "../../features/chat/actions";

const FriendListContainer = styled(Container)`
  .toggle-sort-button {
    margin: 3px;
    text-align: right;
  }

  p {
    text-align: center;
  }
`;

function FriendList() {
  const dispatch = useDispatch();
  const toggle = useSelector(state => state.toggleReducer);
  const chat = useSelector(state => state.chatReducer);
  const userList = Object.assign([], chat.showListData.users);

  const showChatViewHandler = (id) => {
    const roomId = roomData.filter(item => item.users.user_id === id);
    dispatch(showChatView(id, roomId[0].room_id));
  };

  const toggleSortBtnHandler = () => {
    dispatch(toggleSortBtn());
  };

  if (toggle.sortFriendList) {
    userList.sort((a, b) =>
      a.name > b.name ? -1 :
        a.name < b.name ? 1 : 0
    );
  } else {
    userList.sort((a, b) =>
      a.name < b.name ? -1 :
        a.name > b.name ? 1 : 0
    );
  }

  return (
    <FriendListContainer>
      {userList.length !== 0 && <div className="toggle-sort-button">
        <Button
          onClick={() => {toggleSortBtnHandler()}}
        >
          이름 순 {!toggle.sortFriendList ? <AiFillCaretUp /> : <AiFillCaretDown />}
        </Button>
      </div>}
      <Ul>
        {userList.map(item => (
          <List key={item.id}>
            <div className="user-profile">
              <Image src={item.profile} />
              <span className="user-name">{item.name}</span>
            </div>
            <Button
              onClick={() => {showChatViewHandler(item.id)}}
            >
              대화하기
            </Button>
          </List>
        ))}
      </Ul>
      {userList.length === 0 && <p>{chat.showListData}</p>}
    </FriendListContainer>
  );
}

export default FriendList;
