import React from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";

import Container from "../shared/Container";
import Image from "../shared/Image";
import List from "../shared/List";
import Ul from "../shared/Ul";

import roomData from "../../data/room_data.json";

import { showChatView } from "../../features/chat/actions";
import { timeStampToDate } from "../../common/utils";

const ChatList = styled(List)`
  cursor: pointer;
`;

const TextEllipsis = styled.span`
  font-size: 12px;
  width: 300px;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  word-wrap: break-word;
  word-break: break-all;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 1;
`;

const SendDate = styled.span`
  margin: 10px;
  font-size: 12px;
`;

const ChatListContainer = styled(Container)`
  p {
      text-align: center;
  }
`;

function ChattingList() {
  const dispatch = useDispatch();
  const chat = useSelector(state => state.chatReducer);
  const userList = Object.assign([], chat.showListData.users);

  const showChatViewHandler = (id) => {
    const roomId = roomData.filter(item => item.users.user_id === id);
    dispatch(showChatView(id, roomId[0].room_id));
  };

  const filterContent = (id) => {
    return chat.showListData.lastMessageList[id].lastMessages;
  };

  const filterDate = (id) => {
    const timeStamp = chat.showListData.lastMessageList[id].lastMessagesTimeStamp;

    return timeStampToDate(timeStamp).slice(0, 13).trimEnd();
  };

  if (userList.length !== 0) {
    userList.sort((a, b) => {
      const timeStampA = chat.showListData.lastMessageList[a.id].lastMessagesTimeStamp;
      const timeStampB = chat.showListData.lastMessageList[b.id].lastMessagesTimeStamp;

      return timeStampA > timeStampB ? -1 : timeStampA < timeStampB ? 1 : 0;
    });
  }

  return (
    <ChatListContainer>
      <Ul>
        {userList.map(item => (
          <ChatList
            key={item.id}
            onClick={() => {showChatViewHandler(item.id)}}
          >
            <div className="user-profile">
              <Image src={item.profile} />
              <div>
                <span className="user-name">{item.name}</span><br/>
                <TextEllipsis>{filterContent(item.id)}</TextEllipsis>
              </div>
            </div>
            <SendDate>{filterDate(item.id)}</SendDate>
          </ChatList>
        ))}
      </Ul>
      {userList.length === 0 && <p>{chat.showListData}</p>}
    </ChatListContainer>
  );
}

export default ChattingList;
