import React from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import { useSelector } from "react-redux";
import styled from "styled-components";

import Header from "../components/Header/Header";
import ChattingList from "../components/Chat/ChattingList";
import FriendList from "../components/Chat/FriendList";
import Chat from "../components/Chat/Chat";
import NotFound from "../components/NotFound/NotFound";

const Main = styled.div`
  width: 500px;
  height: 800px;
  margin: 0 auto;
  transform: translateY(10%);
  border: 1px solid rgba(44, 44, 44, 0.2);
  border-radius: 10px;
  box-shadow: 1px 2px 2px 2px rgba(44, 44, 44, 0.2);
`;

function App() {
  const chat = useSelector(state => state.chatReducer);

  return (
    <Main>
      {!chat.chatAllData.showChatView &&
        <div>
          <Header />
          <Routes>
            <Route path="/" element={<Navigate to="/friendlist" replace />} />
            <Route path="/friendlist" element={<FriendList />} />
            <Route path="/chatlist" element={<ChattingList />} />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </div>
      }
      {chat.chatAllData.showChatView && <Chat />}
    </Main>
  );
}

export default App;
